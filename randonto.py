#!/usr/bin/env python3

import sys
import random
import math
import string
import subprocess


'''
Randomly generate ontologies.

v0.01 (2021/12/04): simply print an owl ontology in Manchester syntax,
of n atomic concepts and some subset relations among them, randomly
generated with some probability.

v0.02 (2021/12/15): added one individual to populate each atomic
concept; added a parameter to randomly create subset relations between
an atomic concept as the complement of another negative concept,
provided a subset relation between the two atomic concepts was not
already added; created ontologies can now be inconsistent.

v0.03 (2021/12/16): added object properties and subsumption axioms of
atomic concepts with existential concepts.

v0.04 (2022/01/21): saving ontology in owl file; when an app to check
the consistency of an ontology is specified (create one from
https://bitbucket.org/troquard/ontologyutils/src/master/src/main/java/www/ontologyutils/apps/AppCheckConsistencyBasic.java),
stop when a consistent ontology has been found; otherwise generate a
possibly inconsistent ontology.

TODO:
- add nested object properties
- ...

Possible use:
python3 randonto.py 20 3 0.01 0.002 0.001
python3 randonto.py 20 3 0.01 0.002 0.001 checkConsistentOnto.jar

'''

annotation_label = "annotate_axiom"

keyword_subclassof = "SubClassOf"
keyword_class = "Class"
keyword_role = "ObjectProperty"
keyword_individual = "Individual"
keyword_types = "Types"
keyword_objectproperty = "ObjectProperty"
keyword_annotation_property = "AnnotationProperty"
keyword_annotations = "Annotations"
keywords_object_properties = ["some", "only"]
keywords_bool = ["and", "or", "not"]
keywords_axiom = [keyword_subclassof]

prefixes = [  # "Prefix: rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>",
              # "Prefix: rdfs: <http://www.w3.org/2000/01/rdf-schema#>",
              # "Prefix: xsd: <http://www.w3.org/2001/XMLSchema#>",
            "Prefix: owl: <http://www.w3.org/2002/07/owl#>"]


class RandOnto:

    def __init__(self, num_concepts: int, num_roles: int,
                 positive_connectivity_ratio: float,
                 complement_connectivity_ratio: float,
                 existential_concept_connectivity_ratio: float):
        assert(num_concepts > 0)
        assert(num_roles >= 0)
        assert(0.0 <= positive_connectivity_ratio <= 1.0)
        assert(0.0 <= complement_connectivity_ratio <= 1.0)
        assert(0.0 <= existential_concept_connectivity_ratio <= 1.0)
        self.rand_id = random.randint(1, 100000)
        self.num_concepts = num_concepts
        self.num_roles = num_roles
        self.positive_connectivity_ratio = positive_connectivity_ratio
        self.complement_connectivity_ratio = complement_connectivity_ratio
        self.concepts = [":C" +
                         ''.join(random.choices(string.ascii_lowercase,
                                                k=(2 + math.ceil(math.log(num_concepts)))))
                         for i in range(num_concepts)]
        self.existential_concept_connectivity_ratio = existential_concept_connectivity_ratio
        self.roles = [":R" +
                      ''.join(random.choices(string.ascii_lowercase,
                                             k=(2 + math.ceil(math.log(num_roles))))) for i
                      in range(num_roles)]

    def file_name(self):
        return f"C{self.num_concepts}_R{self.num_roles}_{self.positive_connectivity_ratio}_{self.complement_connectivity_ratio}_{self.existential_concept_connectivity_ratio}_{self.rand_id}.owl"

    def ontology_name(self):
        return f"http://example.org/{self.file_name()[0:-4]}.owl"

    def clear_file(self):
        open(self.file_name(), "w").close()

    def make_owl(self):
        with open(self.file_name(), 'a') as out:
            out.write(f"Prefix: : <{self.ontology_name()}#>\n")
            for p in prefixes:
                out.write(f"{p}\n")
            out.write(f"Ontology: <{self.ontology_name()}>\n")
            out.write("\n")

            for c in self.concepts:
                out.write(f"{keyword_individual}: :ind{c[1:]}\n")
                out.write(f"  {keyword_types}: {c}\n")

            for r in self.roles:
                out.write(f"{keyword_role}: {r}\n")

            for c in self.concepts:
                out.write(f"{keyword_class}: {c}\n")
                out.write(f"  {keyword_subclassof}:\n")
                used_as_positive_subset = []
                for c2 in self.concepts:
                    if random.randint(1, 10000) <= 10000 * self.positive_connectivity_ratio:
                        out.write(f"    {c2},\n")
                        used_as_positive_subset.append(c2)
                for c2 in self.concepts:
                    if c2 not in used_as_positive_subset:
                        if random.randint(1, 10000) <= 10000 * self.complement_connectivity_ratio:
                            out.write(f"    not {c2},\n")
                for r in self.roles:
                    for c2 in self.concepts:
                        if random.randint(1, 10000) <= 10000 * self.existential_concept_connectivity_ratio:
                            out.write(f"    ({r} some {c2}),\n")
                    for c2 in self.concepts:
                        if random.randint(1, 10000) <= 10000 * self.existential_concept_connectivity_ratio:
                            out.write(f"    not ({r} some {c2}),\n")
                out.write("    owl:Thing\n")


def main(num_concepts, num_roles, positive_connectivity_ratio,
         complement_connectivity_ratio,
         existential_concept_connectivity_ratio, jarfile=None):
    ro = RandOnto(num_concepts, num_roles,
                  positive_connectivity_ratio,
                  complement_connectivity_ratio,
                  existential_concept_connectivity_ratio)
    print(f"Creating file {ro.file_name()}.")
    if jarfile is None:
        ro.make_owl()
    consistent = False
    while jarfile is not None and not consistent:
        ro.clear_file
        ro.make_owl()
        check_output = subprocess.check_output(["java", "-jar",
                                                jarfile,
                                                ro.file_name()],
                                               encoding='UTF-8')
        consistent = (check_output == "1")
    print(f"Created ontology in {ro.file_name()}.")


def usage(argv):
    if (len(argv) not in [6, 7]):
        print(f"Usage: python3 {argv[0]} <number of concepts> <number of roles> <positive connectivity ratio> <complement connectivity ratio> <existential concept connectivity ratio> [<jar app to check ontology consistency>]")
        print(f"\nThis create an ALC owl ontology in Manchester syntax, with a signature of <number of concepts> atomic concepts and <number of roles> roles. Every atomic class is populated with an individual. Some subset relations among the atomic concepts are randomly generated with roughly the probability <positive connectivity ratio>. Some subset relations between an atomic concept and the negation of an atomic concept are generated with roughly the probability <complement connectivity ratio>. Given two atomic concepts c1 and c2, and a role r, subset relations between c1 and (r some c2) and between c1 and not (r some c2) are generated with probability roughly <existential concept connectivity ratio>.\nFor instance: python3 {argv[0]} 20 3 0.01 0.002 0.001 to generate an ontology in file with 20 atomic concepts, 3 roles, positivity connectivity ratio of 0.01, complement connectivity ratio of 0.002, and existential concept connectivity ratio of 0.001.")
        sys.exit(0)


if __name__ == "__main__":
    usage(sys.argv)
    if len(sys.argv) == 6:
        main(int(sys.argv[1]), int(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]))
    else:
        main(int(sys.argv[1]), int(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]), jarfile=sys.argv[6])
